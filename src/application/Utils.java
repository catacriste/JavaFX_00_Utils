package application;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
/*Para importar esta classe:
 * 1 - No projeo x, registar este projeto:
 * 	   Rato dt� sobre o nome do projeto x - Properties
 * 	   Java Class Path -> Projects - Add
 * 	   (Mostra os projetos do workspace : escoler este) - OK
 *
 * 2 - No project x , escrever: import application.Utils;
 * 	   Os m�todos destas classes j� estar�o dispon�veis.
 * 	   Nota: se o projeto tiver um package diferente,
 * 	   ter� que ser referenciado: import package.Class*/
public class Utils {
	static boolean resposta;
	public static void alertbox(String title, String msg){ //Static para n�o ser instanciada
		
		Stage janela = new Stage();							//Cria uma window
		//janela.initModality(Modality.APPLICATION_MODAL);	//Define uma janela Modal
		janela.initModality(Modality.WINDOW_MODAL);	//Define uma janela Modal
		janela.setTitle(title); 							//Como t�tulo, recebe a string do parametro
		janela.setMinWidth(200);							//Largura da janela
		
		Label mensagem = new Label(msg); 					//Cria a label para mostra
		Button btnClose = new Button("Fechar");				//Cria bot�o para fechar janela
		btnClose.setOnAction(e -> janela.close());			//A��o fecha esta janela
		
		VBox layout = new VBox(10);							//Layout vertical com 10px entre c�lulas
		layout.getChildren().addAll(mensagem, btnClose);	//Adiciona Label e Button ao layout
		layout.setAlignment(Pos.CENTER);					//Alinhar os cnteudos ao Centros
		
		Scene scene = new Scene(layout);					//Criar a Scene e associa o Layout
		janela.setScene(scene);								//Associa a Scena 
		janela.showAndWait();								//Executa e prende o controlo at� ser fechada
		
		
	}
	
	
	
public static boolean confirmationBox(String title, String msg){ //Static para n�o ser instanciada
		
		Stage janela = new Stage();							//Cria uma window
		//janela.initModality(Modality.APPLICATION_MODAL);	//Define uma janela Modal
		janela.initModality(Modality.WINDOW_MODAL);	//Define uma janela Modal
		janela.setTitle(title); 							//Como t�tulo, recebe a string do parametro
		janela.setMinWidth(200);							//Largura da janela
		
		Label mensagem = new Label(msg); 					//Cria a label para mostra
		Button btnTrue = new Button("Sim");				//Cria bot�o para fechar janela
		btnTrue.setOnAction(e -> {
			resposta = true;
			janela.close();			//A��o fecha esta janela
		});
		Button btnFalse = new Button("N�o");
		btnFalse.setOnAction(e -> {
			resposta = false;
			janela.close();			//A��o fecha esta janela
		});
		
		VBox layout = new VBox(10);							//Layout vertical com 10px entre c�lulas
		VBox layout1 = new VBox(10);
		layout.getChildren().addAll(mensagem, layout1);
		layout.setAlignment(Pos.CENTER);
		
		layout1.getChildren().addAll(btnTrue, btnFalse);
	
		
		Scene scene = new Scene(layout);					//Criar a Scene e associa o Layout
		janela.setScene(scene);								//Associa a Scena 
		janela.showAndWait();								//Executa e prende o controlo at� ser fechada
		
		return resposta;
	}
}
